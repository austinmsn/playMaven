### 想了解一下maven 的 寫法 和 基本的job

#### Maven Phases
* mvn validate
* mvn compile
* mvn test
* mvn test-compile
* mvn package
* mvn integration-test
* mvn verify
* mvn install
* mvn deploy

#### other Maven Phases
* mvn clean 
* mvn site

#### pom
* project
* modelVersion
* groupId
* artifactId
* packaging
* version
* name
* url
* description

#### Standard Directory Layout
* src/main/java
* src/main/resources
* src/main/filters
* src/main/webapp
* src/test/java
* src/test/resources
* src/test/filters
* src/it
* src/assembly
* src/site
* LICENSE.txt
* NOTICE.txt
* README.txt

#### properties
* ${project.name}
* ${project.version}
* ${user.home}